package main

import (
	"fmt"

	go_say_hello "gitlab.com/deni.rahmatsyah/go-say-hello"
)

func main() {
	fmt.Println(go_say_hello.MySayHello())
}
